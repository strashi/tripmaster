package tourGuide.dto;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;

public class NearByAttractionDTO implements Comparable<NearByAttractionDTO>{

    public String attractionName;
    public Location attractionLocation;
    public Location userLocation;
    public double distanceInMiles;
    public int rewardPoints;

    public NearByAttractionDTO(Attraction attraction, Location userLocation, double distanceInMiles, int rewardPoints) {
        this.attractionName = attraction.attractionName;
        this.attractionLocation = new Location(attraction.latitude, attraction.longitude);
        this.userLocation = userLocation;
        this.distanceInMiles = distanceInMiles;
        this.rewardPoints = rewardPoints;
    }
    public int compareTo(NearByAttractionDTO nearByAttractionDTO) {

        if (distanceInMiles > nearByAttractionDTO.distanceInMiles) {
            return 1;
        }
        else if (distanceInMiles < nearByAttractionDTO.distanceInMiles) {
            return -1;
        }
        else {
            return 0;
        }

    }



}
